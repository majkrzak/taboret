from networkx import Graph, dfs_preorder_nodes

from . import Strategy


class Brutus(Strategy):

	def __call__(self, G: Graph, p: tuple, e: tuple) -> tuple:
		return self.dfs(G.subgraph(set(G) - {e}), (p,), len(list(dfs_preorder_nodes(G, e))) + 1)

	def dfs(self, G: Graph, path: tuple, depth: int):
		if not depth:
			return path[1]

		for child in G[path[-1]]:
			if child not in path:
				ret = self.dfs(G, (*path, child), depth - 1)
				if ret:
					return ret
