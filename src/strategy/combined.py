from . import Strategy

from networkx import Graph
from time import sleep
from threading import Thread
import ctypes


class StoppableThread(Thread):
	class Exception(BaseException):
		pass

	def __init__(self, *args, **kwargs):
		super().__init__(*args, **kwargs)

	def run(self):
		try:
			self._return = self._target(*self._args, **self._kwargs)
		except StoppableThread.Exception:
			print('StoppableThread.Exception')

	def join(self) -> tuple:
		if self.isAlive():
			ctypes.pythonapi.PyThreadState_SetAsyncExc(
				ctypes.c_long(self.ident),
				ctypes.py_object(StoppableThread.Exception())
			)
		else:
			super().join()
			return self._return


class Combined(Strategy):
	timeout: float
	strategies: list

	def __init__(self, timeout: float, *strategies):
		self.timeout = timeout
		self.strategies = strategies

	def __call__(self, G: Graph, p: tuple, e: tuple) -> tuple:
		threads = [StoppableThread(target=strategy, args=(G, p, e)) for strategy in self.strategies]

		for thread in threads:
			thread.start()

		sleep(self.timeout)

		results = [thread.join() for thread in threads]

		return next((result for result in results if result))
