from .strategy import Strategy

from .brutus import Brutus
from .combined import Combined
from .conditional import Conditional
from .mindeg import Mindeg
from .minimax import Minimax
from .random import Random
