from . import Strategy

from networkx import Graph, bfs_edges
from random import random


class Mindeg(Strategy):
	level: int

	def __init__(self, level: int):
		self.level = level

	def __call__(self, G: Graph, p: tuple, e: tuple) -> tuple:
		Gp = G.subgraph({*G} - {e})
		Np = [n for n in Gp.neighbors(p) if G.degree(n) > 1] or [n for n in G.neighbors(p)]
		return min(Np, key=lambda n: random() / 512 + len(list(bfs_edges(Gp, n, False, self.level))))
