from . import Strategy

from networkx import Graph
from random import choice


class Random(Strategy):

	def __call__(self, G: Graph, p: tuple, e: tuple) -> tuple:
		return choice(list(G.subgraph(set(G) - {e}).neighbors(p)) or [e])
