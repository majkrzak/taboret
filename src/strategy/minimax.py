from . import Strategy

from networkx import Graph


class Minimax(Strategy):
	score: None
	level: int

	def __init__(self, score: None, level: int):
		self.score = score
		self.level = level

	def __call__(self, G: Graph, p: tuple, e: tuple) -> tuple:
		return self._eval(G, p, e, self.level)[1]

	def _eval(self, G: Graph, p: tuple, e: tuple, depth: int) -> tuple:
		if p not in G or not len(G[p]) or not depth:
			return self.score(G, p, e), None

		return max((-self._eval(G.subgraph(set(G) - {p}), e, m, depth - 1)[0], m) for m in G.neighbors(p))
