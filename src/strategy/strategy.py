from abc import ABCMeta, abstractmethod
from networkx import Graph


class Strategy(metaclass=ABCMeta):

	@abstractmethod
	def __call__(self, G: Graph, p: tuple, e: tuple) -> tuple:
		pass
