from . import Strategy

from networkx import Graph


class Conditional(Strategy):
	condition: None
	positive: Strategy
	negative: Strategy

	def __init__(self, condition: None, positive: Strategy, negative: Strategy):
		self.condition = condition
		self.positive = positive
		self.negative = negative

	def __call__(self, G: Graph, p: tuple, e: tuple) -> tuple:
		return self.positive(G, p, e) if self.condition(G, p, e) else self.negative(G, p, e)
