import grpc
from concurrent import futures
import time

# import the generated classes
from . import player_pb2
from . import player_pb2_grpc

from networkx import Graph
import datetime


class PlayerServicer(player_pb2_grpc.PlayerServicer):

    def __init__(self, strategy):
        self.strategy = strategy

    def NextMove(self, request, context):
        # f1 = open(f'/logs/{datetime.datetime.today().strftime("%Y-%m-%dT")}.txt', 'w+')
        # f1 = open('/logs/testfile10.txt', 'a')

        G = Graph()
        p = None
        e = None

        me = request.playerNumber
        for tile in request.tilesCollection:
            node = (tile.position.x, tile.position.y)

            if tile.playerHere:
                if tile.playerHere == me:
                    p = node
                else:
                    e = node
            elif tile.wallOfPlayer:
                # print('wall', node, file=f1)
                continue

            G.add_node(node)

            for neighbour in (
                    (node[0] + 1, node[1]),
                    (node[0], node[1] + 1),
                    (node[0] - 1, node[1]),
                    (node[0], node[1] - 1)
            ):
                if neighbour in G:
                    G.add_path((neighbour, node))

        m = self.strategy(G, p, e)
        # print('m', m, file=f1)

        # f1.close()

        response = player_pb2.Position()
        response.x = m[0]
        response.y = m[1]
        return response


class Server:

    def __init__(self, strategy):
        # create a gRPC server
        self.server = grpc.server(futures.ThreadPoolExecutor(max_workers=10))

        # use the generated function `add_PlayerServicer_to_server`
        # to add the defined class to the server
        player_pb2_grpc.add_PlayerServicer_to_server(PlayerServicer(strategy), self.server)

    def __call__(self):
        # listen on port 50051
        print('Starting server. Listening on port 50051.')
        self.server.add_insecure_port('[::]:50051')
        self.server.start()

        # since server.start() will not block,
        # a sleep-loop is added to keep alive
        try:
            while True:
                time.sleep(86400)
        except KeyboardInterrupt:
            self.server.stop(0)
