from .server import Server
from . import strategy
from . import score
from networkx import has_path, dfs_preorder_nodes
from sys import argv

strategies = {
	's1': strategy.Conditional(
		lambda G, p, e: len(G[p]) > 1,
		strategy.Conditional(
			has_path,
			strategy.Minimax(score.node_dominance, 4),
			strategy.Mindeg(2)
		),
		strategy.Random(),
	),
	's2': strategy.Conditional(  # chujnia
		lambda G, p, e: len(G[p]) > 1,
		strategy.Conditional(
			has_path,
			strategy.Minimax(score.edge_dominance, 2),
			strategy.Conditional(
				lambda G, p, e: len(list(dfs_preorder_nodes(G.subgraph({*G} - {p}), e))) > 16,
				strategy.Mindeg(2),
				strategy.Brutus()
			)
		),
		strategy.Random(),
	),
	's3': strategy.Conditional(
		lambda G, p, e: len(G[p]) > 1,
		strategy.Conditional(
			has_path,
			strategy.Minimax(score.node_dominance, 4),
			strategy.Conditional(
				lambda G, p, e: len(list(dfs_preorder_nodes(G.subgraph({*G} - {p}), e))) > 16,
				strategy.Mindeg(2),
				strategy.Brutus()
			)
		),
		strategy.Random(),
	),
	't3': strategy.Combined(
		4.5,
		strategy.Conditional(
			lambda G, p, e: len(G[p]) > 1,
			strategy.Conditional(
				has_path,
				strategy.Minimax(score.node_dominance, 4),
				strategy.Conditional(
					lambda G, p, e: len(list(dfs_preorder_nodes(G.subgraph({*G} - {p}), e))) > 16,
					strategy.Mindeg(2),
					strategy.Brutus()
				)
			),
			strategy.Random(),
		),
		strategy.Random()
	)
}

Server(strategies[argv[1] if len(argv) > 1 else 's1'])()
