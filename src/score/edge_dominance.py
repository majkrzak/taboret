from networkx import Graph, has_path, dfs_preorder_nodes, single_source_shortest_path_length
from math import inf


def edge_dominance(G: Graph, p: tuple, e: tuple) -> float:
	if p not in G:
		return -inf
	if e not in G:
		return inf
	if p == e:
		return 0.

	if not has_path(G, p, e):
		Gdp = G.subgraph(dfs_preorder_nodes(G, p))
		Gde = G.subgraph(dfs_preorder_nodes(G, e))
		return Gdp.number_of_nodes() - Gde.number_of_nodes()
	else:
		Dp = dict(single_source_shortest_path_length(G, p))
		De = dict(single_source_shortest_path_length(G, e))
		Gdp = G.subgraph((node for node in G if node in Dp and (node not in De or Dp[node] < De[node])))
		Gde = G.subgraph((node for node in G if node in De and (node not in Dp or De[node] < Dp[node])))
		return (Gdp.number_of_edges() - Gde.number_of_edges()) / (Gde.number_of_edges() + Gdp.number_of_edges())
