FROM python:3.7-slim
COPY . .
RUN python3 setup.py bdist_wheel --dist-dir .
RUN pip3 install taboret-0.0.0-py3-none-any.whl
ENTRYPOINT ["python3", "-m", "taboret"]
CMD []
