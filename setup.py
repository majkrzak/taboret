from setuptools import setup

setup(
	name='taboret',
	description='Best food spy ever',
	author='Piotr Majrzak',
	author_email='petrol.91@gmail.com',
	license='Permission to use, copy, modify, and distribute this software for any '
	        + 'purpose with or without fee is hereby granted, provided that the above '
	        + 'copyright notice and this permission notice appear in all copies.',
	packages=[
		'taboret',
		'taboret' + '.score',
		'taboret' + '.strategy',
		'taboret' + '.server'
	],
	package_dir={'taboret': './src'},
	install_requires=[
		'networkx',
		'grpcio',
		'google',
		'protobuf'
	],
	setup_requires=[
		'wheel'
	]
)
